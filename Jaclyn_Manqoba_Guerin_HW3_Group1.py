from subprocess import call
import numpy as np
import matplotlib.pyplot as plt
from pdb import set_trace as trace
from scipy.integrate import simps as simpson

KNIFE_SCREEN_DISTANCE = 10 #units of meters
WAVELENGTH = 632.9e-9 #units of meters
VERTICAL_RANGE = np.linspace(-8.5e-3, 8.5e-3, 1000) #units of meters
INTENSITY_ARG_RANGE = VERTICAL_RANGE * np.sqrt(2/(WAVELENGTH * KNIFE_SCREEN_DISTANCE))

def clear():
    call('clear')
    return
clear()

def S_fresnel_deriv_4(x):
    c = np.pi/2.
    term_1 = 16. * (c**4) * (x**4) * np.sin(c * (x**2))
    term_2 = 48. * (c**3) * (x**2) * np.cos(c * (x**2))
    term_3 = 12. * (c**2) * np.sin(c * (x**2))
    return term_1 - term_2 - term_3

def C_fresnel_deriv_4(x):
    c = np.pi/2.
    term_1 = 16. * (c**4) * (x**4) * np.cos(c * (x**2))
    term_2 = 48. * (c**3) * (x**2) * np.sin(c * (x**2))
    term_3 = 12. * (c**2) * np.cos(c * (x**2))
    return term_1 + term_2 - term_3

def S_fresnel_integrand(w):
    return np.sin((np.pi/2)*(w**2))

def C_fresnel_integrand(w):
    return np.cos((np.pi/2)*(w**2))

def error_eval(func, limits_arg, point):
    coeff = (1. / 90.) * (((limits_arg[1] - limits_arg[0]) / 2)**5)
    return coeff * abs(func(*(point,)))
"""
def Simpsons_rule(func, limits_arg):
    lower_point = limits_arg[0]
    middle_point = (limits_arg[1] - limits_arg[0]) / 2.
    upper_point = limits_arg[1]
    coeff = (upper_point - lower_point) / 6.
#    trace()
    return coeff * (func(*(lower_point,)) + 4.*func(*(middle_point,)) + func(*(upper_point,)))

def extended_simpson(func, limits_arg, N):
    step_size = limits_arg[1] - limits_arg[0]/float(N)
    func_vals = []
    func_vals.append(func(*(limits_arg[0],)))
    func_vals.append(func(*(limits_arg[1],)))
    for k in range(1, N, 2):
        arg = limits_arg[0]+(k*step_size)
        func_vals.append(4.*func(*(arg,)))
    for i in range(2, N, 2):
        arg = limits_arg[0]+(i*step_size)
        func_vals.append(2.*func(*(arg,)))
    return (step_size/3.)*sum(func_vals)
"""
def extended_simpson(f, a, b, n):
    h=(b-a)/n
    k=0.0
    x=a + h
    for i in range(1,n/2 + 1):
        k += 4*f(*(x,))
        x += 2*h

    x = a + 2*h
    for i in range(1,n/2):
        k += 2*f(*(x,))
        x += 2*h
    return (h/3)*(f(*(a,))+f(*(b,))+k)

def Better_Simpson_rule(func, limits_arg):
    return simpson(func(*(np.linspace(limits_arg[0], limits_arg[1], 100),)))

def C_fresnel(v, N=2):
    return extended_simpson(C_fresnel_integrand, 0, v, N)

def S_fresnel(v, N=2):
    return extended_simpson(S_fresnel_integrand, 0, v, N)

def intensity(v):
    return ((C_fresnel(v) + 0.5)**2) + ((S_fresnel(v) + 0.5)**2)


if __name__  == '__main__':
    plt.plot(VERTICAL_RANGE, intensity(INTENSITY_ARG_RANGE))
    plt.xlabel("Vertical Distance in m")
    plt.ylabel("Intensity")
    plt.show()


