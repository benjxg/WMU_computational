import numpy as np
import numpy.linalg as linalg
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches

k = []
eg1 = []
eg2 = []
eg3 = []
eg4 = []
eg5 = []
eg6 = []

#OK IT IS TIME MOTHAFUCKA
for g in range(1,375):
    ratio = .25+g*.01
    F2 = -.1*np.array([[ratio+1,-ratio,0,0,0,0],
    [-ratio,ratio+1,-1,0,0,0],
    [0,-1,ratio+1,-ratio,0,0],
    [0,0,-ratio,ratio+1,-1,0],
    [0,0,0,-1,ratio+1,-ratio],
    [0,0,0,0,-ratio,ratio+1]])
    x = linalg.eigvalsh(F2)
    k.append(ratio)
    eg1.append(np.sqrt(abs(x[0])))
    eg2.append(np.sqrt(abs(x[1])))
    eg3.append(np.sqrt(abs(x[2])))
    eg4.append(np.sqrt(abs(x[3])))
    eg5.append(np.sqrt(abs(x[4])))
    eg6.append(np.sqrt(abs(x[5])))

plt.plot(k,eg1, "r-")
plt.plot(k,eg2, "m-")
plt.plot(k,eg3, "y-")
plt.plot(k,eg4, "g-")
plt.plot(k,eg5, "b-")
plt.plot(k,eg6, "k-")
eig1_patch = mpatches.Patch(color='red', label='eig 1')
eig2_patch = mpatches.Patch(color='magenta', label='eig 2')
eig3_patch = mpatches.Patch(color='yellow', label='eig 3')
eig4_patch = mpatches.Patch(color='green', label='eig 4')
eig5_patch = mpatches.Patch(color='blue', label='eig 5')
eig6_patch = mpatches.Patch(color='black', label='eig 6')
plt.legend(handles=[eig1_patch,eig2_patch,eig3_patch,eig4_patch,eig5_patch,eig6_patch])
plt.xlabel("K ratio")
plt.ylabel("Frequency")
plt.show()
