"""
Jaclyn Brett
Manqoba
Ben Guerin

This program evaluates intensity on a screen from a laser diffracting
off of a knife edge. It runs slowly because, in order to get the error
of each integral at every point, we take the difference of the integral
evaluation for N steps and N-1 steps. We had included a way to give the
tolerance as an input of the program but this was even slower. We found
a value of N such that a sufficiently low error is found but it is quite
large. If an error is above the assigned tolerance (4 significant figures)
the error value is printed. If nothing prints, all errors are below the
threshold.

Values of the integrals were checked against a table of Fresnel integral
values and Wolfram Alpha's FresnelC[z] and FresnelS[z] functions.

Table of Fresnel integral values:
http://www.dwc.knaw.nl/DL/publications/PU00011466.pdf
"""

from math import pi
from pylab import plot, show, xlabel, ylabel, annotate, title
from numpy import sin, cos, sqrt, linspace, concatenate, amax

WAVELENGTH = 632.9e-9 #Wavelength of laser in meters
KNIFE_SCREEN_DISTANCE = 10. #Distance between knife edge and screen in meters
VERTICAL_RANGE = linspace(-8.5e-3, 8.5e-3, 1000) #Vertical range of screen to be plotted in meters
INTENSITY_ARG_RANGE = VERTICAL_RANGE*sqrt(2.0/(WAVELENGTH*KNIFE_SCREEN_DISTANCE)) #Unitless range of arguments for Intensity function
INTEGRAL_STEPS = 70 #Number of divisions to use for Simpsons rule. Quite large but this was the only way to get the error down

# Fresnel functions
def C_fresnel_integrand(w):
    """
    Integrand of the cosine Fresnel function. This
    takes an integer or float as an argument and
    returns a float.
    """
    return cos(pi*(w**2.)/2.)

def S_fresnel_integrand(w):
    """
    Integrand of the sine Fresnel function. This
    takes an integer or float as an argument and
    returns a float.
    """
    return sin(pi*((w**2.)/2.0))

#computing Fresnel integrals using Simpson's rule
def extended_simpson(func, upper_limit, intervals=INTEGRAL_STEPS):
    """
    We wrote a general simpson function so that
    we could use it for any function in the future.
    Currently, it does assume that the lower limit
    of the integral is 0 but that is easily changed.
    This function returns a float.

    Arguments:
        func - the function to be integrated

        upper_limit - the upper limit of the integral

        intervals - the number of intervals to use Simpson's
        rule on. The default value (INTEGRAL_STEPS)
        is set at the beginning of the program.

    """
    step_size = upper_limit/float(intervals)
    func_extremes = (step_size/3.0) * (func(*(upper_limit,)) + func(*(0,)))
    other_vals = 0
    for k in range(1, intervals/2):
        other_vals += (step_size/3.0) * (4*func(*(step_size*(2*k-1),)) + 2*func(*(2*k*step_size,)))
    other_vals += (step_size/3.0) * 4 * func(*(step_size*(intervals-1),))
    return other_vals + func_extremes

#Intensity observed on the screen
def Intensity(v):
    """
    This function uses the Fresnel integrals to give the intensity.
    It takes v as an argument where v is some integer or float and
    is related to vertical screen position by:
    v = VERTICAL_RANGE*sqrt(WAVELENGTH/KNIFE_SCREEN_DISTANCE)
    It returns a tuple: (intensity values, values from Cfresnel
    integral, values from Sfresnel integral)
    """
    C_fresnel_vals = extended_simpson(C_fresnel_integrand, v)
    S_fresnel_vals = extended_simpson(S_fresnel_integrand, v)
    return ((C_fresnel_vals+0.5)**2 + (S_fresnel_vals+0.5)**2, C_fresnel_vals, S_fresnel_vals)




if __name__ == "__main__":

    intensity = Intensity(INTENSITY_ARG_RANGE)
    C_fresnel_values = intensity[1]
    S_fresnel_values = intensity[2]
    C_errors = abs(C_fresnel_values - extended_simpson(C_fresnel_integrand, INTENSITY_ARG_RANGE, intervals=(INTEGRAL_STEPS - 2)))
    S_errors = abs(S_fresnel_values - extended_simpson(S_fresnel_integrand, INTENSITY_ARG_RANGE, intervals=(INTEGRAL_STEPS - 2)))
    errors = concatenate((C_errors, S_errors), axis=0)

    #Now check if any errors are above the threshold
    for error in errors:
        if error > .0001:
            print "Error value ", error, "too large"
    print "Largest error is: ", amax(errors)
    plot(VERTICAL_RANGE, intensity[0])
    xlabel("Vertical Screen Position (meters)")
    ylabel("Intensity")
    show()
    print "This is what we expect based on the photo included."
