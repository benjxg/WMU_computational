"""
Homework 1
Group 2
Michael Bischak
Ben Guerin
Digamma Function
"""

from numpy import log
from pylab import plot, show, xlabel, ylabel
#Part a
print "This is the start of a"

#give the users the chance to define their z and their k
#z = float(input("z: "))

#error = float(input("error: "))

def digamma(digamma_index, T):
    """
    This function evaluates the digamma function by using
    a sum. It takes two number arguments(float or int). digamma_index
    is the input number for the digamma function. T is the
    error that bounds the sum. This continuously sums and
    checks the size of the error on the sum until the error
    is smaller than the input error. It returns a float.
    """
    #This is the starting index, this will change
    k = 1
    #This is the summing variable. It always starts with negative gamma
    dsum = -0.577215664901533

    #here is the real gamma function without the sum
    def digammafunction(k):
        """
        This function is the internal function that gets
        summed over to evaluate the digamma function. It
        takes one number (float or int) argument. k is
        the index that is the 'current' index in the sum.
        It returns a float.
        """
        L = (digamma_index-1)/((1+k)*(digamma_index+k))
        return float(L)

    def Terror(k):
        """
        This function evaluates the error for the current
        index of the sum. Until this value is less than the
        input value, the sum continues. It takes one number
        argument (int or float) and returns a float.
        """
        Terror = log(2*k/(k+1))-log((2*k+1)/(k+2))-(digamma_index-1)/((2+k)*(digamma_index+k+1))
        return float(Terror)

    while Terror(k) > T:
        #Loop and add to the sum value until the error is less than required
        k += 1
        Terror(k)

    #Here is where we perform the actual sum
    for i in range(0, k+1):
        dsum += digammafunction(i)
    return float(dsum)

#digamma(z, error)
print digamma(.5, .01), '\n', '\n'
print "this is the beginning of part b!"
#b
#define the expression
def infinite_sum(j):
    """
    This evaluates a single term for the second sum
    given in the homework for Group 2, part 3. It is
    given one number argument (float or int), j for the index of the
    sum. It returns a float.
    """
    W = 1/((2.*j+1.)*(j-1./2.)*(j+3./2.))
    return W

#create a number that the sum will be stored in
infinite_sum_value = 0
n = 1
def T2error(c):
    """
    This function finds the error for the sum given in part 3.
    It takes one number argument (float or int). The error it
    finds is for the given index n.
    """
    T2error_value = (1./4.)*(2.*log(2.*c+1.)-log(4*c*(n+1.)-3.))-(1./4.)*(2.*log(2.*c+3.)-log(4.*c**2.+12.*c+5.))-1/((2.*c+3.)*(c+1./2.)*(c+5./2.))
    return T2error_value

#estimation of the truncation error
current_sum_error = T2error(n)

while current_sum_error > .0000001:
    n += 1
    current_sum_error = T2error(n)

print "n is:", n, "T2error is: ", T2error(n)

for p in range(1, n+1):
    infinite_sum_value += infinite_sum(p)
print "the sum is: ", infinite_sum_value, '\n', '\n'
print"This is the beginning of C"

#c
#I have found that we are looking for -1/2*DIG(1/2)+1/2*DIG(3/4) where
#DIG is the digamma function
print "The answer to C is: ", -.5*digamma(.5, .01)+.5*digamma(.75, .01), '\n', '\n'
print "This is the beginning of part d"

#d
#so we have to plot digamma_index between .01 to 1 and 1 to 30
shorter_digamma_output = []
shorter_digamma_arguments = []

for a in range(1, 101, 1):
    shorter_digamma_output.append(digamma(a*.01, .01))
    shorter_digamma_arguments.append(a*.01)

plot(shorter_digamma_arguments, shorter_digamma_output)
xlabel(".01 <= z <= 1")
ylabel("Digamma(z)")
show()

longer_digamma_output = []
longer_digamma_arguments = []

for b in range(1, 301, 1):
    longer_digamma_output.append(digamma(b*.1, .01))
    longer_digamma_arguments.append(b*.1)

plot(longer_digamma_arguments, longer_digamma_output)
xlabel("1 <= z <= 30")
ylabel("Digamma(z)")
show()
