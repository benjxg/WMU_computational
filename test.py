def f(x, y):
    return x**y

def tester(g, z):
    return g(*z)

print tester(f, (2,3))
