var searchData=
[
  ['newtons_5fmethod',['Newtons_method',['../namespaceBhatt__Guerin__Group__2__Homework__2.html#ae3a9daf6608056372ed853e0b58f3191',1,'Bhatt_Guerin_Group_2_Homework_2']]],
  ['nonlinear_5fbook',['nonlinear_book',['../namespaceBhatt__Guerin__Group__2__Homework__2.html#a7684af3cb99062d33e885cbed542152d',1,'Bhatt_Guerin_Group_2_Homework_2']]],
  ['nonlinear_5fbook_5fderivative_5fv1',['nonlinear_book_derivative_v1',['../namespaceBhatt__Guerin__Group__2__Homework__2.html#a514913f7f472890fab4ad0600c662b69',1,'Bhatt_Guerin_Group_2_Homework_2']]],
  ['nonlinear_5fbook_5fderivative_5fv2',['nonlinear_book_derivative_v2',['../namespaceBhatt__Guerin__Group__2__Homework__2.html#a33dcbdb203f4588391b0d4bdde6c1eda',1,'Bhatt_Guerin_Group_2_Homework_2']]],
  ['nonlinear_5fderived',['nonlinear_derived',['../namespaceBhatt__Guerin__Group__2__Homework__2.html#a67f5bb629b4a47d02e5c3c882fc9c519',1,'Bhatt_Guerin_Group_2_Homework_2']]],
  ['nonlinear_5fderived_5fderivative_5fv1',['nonlinear_derived_derivative_v1',['../namespaceBhatt__Guerin__Group__2__Homework__2.html#af1d9dbda4db370268042a42e9a38cea9',1,'Bhatt_Guerin_Group_2_Homework_2']]],
  ['nonlinear_5fderived_5fderivative_5fv2',['nonlinear_derived_derivative_v2',['../namespaceBhatt__Guerin__Group__2__Homework__2.html#ac4e608039d576b2bf77b92ab895e7c79',1,'Bhatt_Guerin_Group_2_Homework_2']]]
];
