"""
Khushi Bhatt
Ben Guerin
This program determines voltages given by a nonlinear equation
on the extremes of the diode pictured in the book for exercise
6.17 on page 277.

This program should be relatively easy to generalize for
finding the root of more than two functions of two
variables.

In this program, functions are unnecessarily defined when variables
would do just as well. The reason for this is for clarity when reading
the code and it made debugging easier. This whould be changed if program
speed were an issue since calling too many functions can slow things
down. However, this program finds an accurate solution in very few
iterations and optimizing speed is unnecessary.
"""

#Import modules
from inspect import getsource as source
import numpy as np

#The number of iterations for Newton's method
#Using ~40 iterations or more gives an error to be zero for both
#variables according to the computer.
ITERATIONS = 40

#Units of Ohms
RESISTOR_1 = 1.e3
RESISTOR_2 = 4.e3
RESISTOR_3 = 3.e3
RESISTOR_4 = 2.e3

#Units of Volts
VOLTAGE_PLUS = 5. #Defined in the book as V+
VOLTAGE_CONSTANT = .05 #Defined in the book as V_T

#Units of A
CURRENT_CONSTANT = 3.e-9

#The initial guess for the voltages is below
#The wrong inital guesses can mess up the program.
#If one is curious, switch the negative sign to the other element.
voltage_guess = (-5., 5.)

def nonlinear_book(voltages):
    """
    This function is the one given in the book. This is one of the functions
    that we want to find the root of. It takes an argument that must be some
    iterable object of length at least two. If longer, elements after the second
    are unused. The function returns a float.
    """
    term_1 = (voltages[0] - VOLTAGE_PLUS) / (RESISTOR_1)
    term_2 = voltages[0] / RESISTOR_2
    term_3 = CURRENT_CONSTANT * (np.exp((voltages[0] - voltages[1]) / VOLTAGE_CONSTANT) - 1)
    return float(term_1 + term_2 + term_3)

def nonlinear_derived(voltages):
    """
    This function is the one we derived using Kirchoff's laws. This is the other
    function that we want to find the root of. It takes an argument that
    must be some iterable object of length at least two. If longer, elements
    after the second are unused. The function returns a float.

    The tuple called 'voltages' has its first element as V_1 and the second
    element is V_2.
    """
    first_term = (voltages[1] - VOLTAGE_PLUS)/RESISTOR_3
    second_term = voltages[1]/RESISTOR_4
    third_term = CURRENT_CONSTANT * (np.exp((voltages[0] - voltages[1]) / VOLTAGE_CONSTANT) - 1)

    return first_term + second_term - third_term


def nonlinear_book_derivative_v1(voltages):
    """
    This function is the derivative with respect to the first voltage of the
    book's equation. It takes an argument that must be some iterable object
    of length at least two. If longer, elements after the second are unused.
    The function returns a float.
    """
    f_term = 1 / RESISTOR_1
    s_term = 1 / RESISTOR_2
    t_term = (CURRENT_CONSTANT / VOLTAGE_CONSTANT) * np.exp((voltages[0] - voltages[1]) / VOLTAGE_CONSTANT)
    return f_term + s_term + t_term

def nonlinear_book_derivative_v2(voltages):
    """
    This function is the derivative with respect to the second voltage of the
    book's equation. It takes an argument that must be some iterable object
    of length at least two. If longer, elements after the second are unused.
    The function returns a float.
    """
    return -1*(CURRENT_CONSTANT / VOLTAGE_CONSTANT) * np.exp((voltages[0] - voltages[1]) / VOLTAGE_CONSTANT)

def nonlinear_derived_derivative_v1(voltages):
    """
    This function is the derivative with respect to the first voltage of the
    derived equation. It takes an argument that must be some iterable object
    of length at least two. If longer, elements after the second are unused.
    The function returns a float.
    """
    return -1 * (CURRENT_CONSTANT / VOLTAGE_CONSTANT) * np.exp((voltages[0] - voltages[1]) / VOLTAGE_CONSTANT)


def nonlinear_derived_derivative_v2(voltages):
    """
    This function is the derivative with respect to the second voltage of the
    derived equation. It takes an argument that must be some iterable object
    of length at least two. If longer, elements after the second are unused.
    The function returns a float.
    """
    term_f = 1 / RESISTOR_3
    term_s = 1 / RESISTOR_4
    term_t = (CURRENT_CONSTANT / VOLTAGE_CONSTANT) * np.exp((voltages[0] - voltages[1]) / VOLTAGE_CONSTANT)
    return term_f + term_s + term_t


def Newtons_method(starting_guess):
    """
    This function run's Newton's method once. The equation used for the method is given
    on page 276 of Newman's book. The argument of this function must be some iterable object.
    If the object has more than two elements, any elements passed the second are ingored. The
    function returns a tuple of length 2. This tuple is the new guess for the next iteration.
    """
    nonlinear_func_array = np.array([nonlinear_book(starting_guess), nonlinear_derived(starting_guess)])

    nonlinear_derivatives_array = np.array([[nonlinear_book_derivative_v1(starting_guess),
                                             nonlinear_book_derivative_v2(starting_guess)],
                                            [nonlinear_derived_derivative_v1(starting_guess),
                                             nonlinear_derived_derivative_v2(starting_guess)]])
    nda = nonlinear_derivatives_array #redefining variable name for cleanness

    inversion_factor = (1/(((nda[0][0]) * (nda[1][1])) - ((nda[0][1]) * (nda[1][0]))))
    inverse_derivative_array = inversion_factor * np.array([[nda[1][1], -1 * nda[0][1]],
                                                            [-1 * nda[1][0], nda[0][0]]])
    delta_x = np.dot(np.mat(nonlinear_func_array), np.mat(inverse_derivative_array))
    new_guess = np.array(starting_guess - delta_x)
    new_guess = (new_guess[0][0], new_guess[0][1])
    return  new_guess

#Do the iteration of Newton's method a set number of times
for k in range(ITERATIONS):
    old_guess = voltage_guess
    voltage_guess = Newtons_method(voltage_guess)
    error = np.array(old_guess) - np.array(voltage_guess)

voltage_drop = voltage_guess[0] - voltage_guess[1]

print "Part a: \nOur derived function is: \n"
print source(nonlinear_derived), "\n"

print "Part b: \nRunning newton's method over ", ITERATIONS, "iterations gives: "
print "V_1 = ",voltage_guess[0], "\nV_2 = ",voltage_guess[1],"\n"

print "Part c: \nVoltage drop across the diode is: ", voltage_drop
print "This is consistent with the 'electronic engineer's rule of thumb'"
print "that the voltage drop across a forward biased diode is ~0.6V\n"

print "Error: \n Page 271 of Newman's Computational book justifies"
print "an appropriate error approximation as error = x - x'"
print "where x - x' is the difference between the last two"
print "guesses. The approximate error on the results here is: "
print "Voltage 1 error: ", abs(error[0])
print "Voltage 2 error: ", abs(error[1])
print "Depending on the number of iterations, this may be"
print "beyond the computers limits to record and the result"
print "will be zeros in that case."
