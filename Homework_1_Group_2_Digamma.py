from pdb import set_trace as trace
import matplotlib.pyplot as plt
import numpy as np


euler_mascheroni_const = 0.577215664901533

def single_digamma_term(z, k):
    term = (z - 1.)/((1. + k)*(z + k))
    return term

def digamma(z, maximum=100000, error=0):
    sum_value = -1*euler_mascheroni_const
    k = 0
    while k < maximum:
        current_term = (z - 1.)/((1. + k)*(z + k))
        sum_value += current_term
        k += 1
    return sum_value

def single_j_sum_term(j):
    return 1/((2*j + 1.)*(j - .5)*(j + 1.5))

def j_sum(maximum=100000, error=0):
    sum_value = 0
    j = 0
    while j <= maximum:
        sum_value += 1/((2*j + 1.)*(j - .5)*(j + 1.5))
        j += 1
    return sum_value
